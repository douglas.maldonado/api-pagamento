package br.com.pagamento.pagamento.clients;

import br.com.pagamento.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CartaoClient {

    @GetMapping("/cartao/{numero}")
    Cartao getCartaoByNumero(@PathVariable Long numero);

    @GetMapping("/cartao/id/{id}")
    Cartao getCartaoById(@PathVariable Long id);
}
