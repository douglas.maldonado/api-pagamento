package br.com.pagamento.pagamento.clients;

import br.com.pagamento.pagamento.models.Cartao;

public class CartaoClientFallback implements CartaoClient {
    @Override
    public Cartao getCartaoByNumero(Long numero) {
        //configurar resposta ou ações a tomar
        return null;
    }

    @Override
    public Cartao getCartaoById(Long id) {
        //configurar respostas ou ações a tomar
        return null;
    }
}
