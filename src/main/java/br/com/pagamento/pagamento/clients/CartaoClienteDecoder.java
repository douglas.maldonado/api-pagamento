package br.com.pagamento.pagamento.clients;

import br.com.pagamento.pagamento.exceptions.CartaoNotFound;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClienteDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new CartaoNotFound();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
