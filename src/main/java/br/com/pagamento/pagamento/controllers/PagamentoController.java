package br.com.pagamento.pagamento.controllers;

import br.com.pagamento.pagamento.models.Pagamento;
import br.com.pagamento.pagamento.models.dtos.EntradaDTOPagamento;
import br.com.pagamento.pagamento.models.dtos.RespostaDTOPagamento;
import br.com.pagamento.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaDTOPagamento cadastrarPagamento (@RequestBody @Valid EntradaDTOPagamento entradaDTOPagamento){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(entradaDTOPagamento.getDescricao());
        pagamento.setValor(entradaDTOPagamento.getValor());
        pagamento.setCartao_id(entradaDTOPagamento.getCartao_id());

        Pagamento pagamentoObjeto = pagamentoService.salvarPagamento(pagamento);

        RespostaDTOPagamento respostaDTOPagamento = new RespostaDTOPagamento();
        respostaDTOPagamento.setId(pagamentoObjeto.getId());
        respostaDTOPagamento.setCartao_id(pagamentoObjeto.getCartao_id());
        respostaDTOPagamento.setDescricao(pagamentoObjeto.getDescricao());
        respostaDTOPagamento.setValor(pagamentoObjeto.getValor());

        return respostaDTOPagamento;
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public List<RespostaDTOPagamento> consultarPagamentos(@PathVariable(name = "id_cartao") Long id){
        List<Pagamento> pagamentos = new ArrayList<>();
        Iterable<Pagamento> pagamentoIterable = pagamentoService.consultarPagamentosPorCartao(id);

        pagamentoIterable.forEach(pagamentos::add);

        List<RespostaDTOPagamento> respostaDTOPagamentos = new ArrayList<>();

        for(Pagamento pagamento: pagamentos) {
            RespostaDTOPagamento respostaDTOPagamento = new RespostaDTOPagamento();
            respostaDTOPagamento.setId(pagamento.getId());
            respostaDTOPagamento.setCartao_id(pagamento.getCartao_id());
            respostaDTOPagamento.setDescricao(pagamento.getDescricao());
            respostaDTOPagamento.setValor(pagamento.getValor());

            respostaDTOPagamentos.add(respostaDTOPagamento);
        }

        return respostaDTOPagamentos;
    }
}
