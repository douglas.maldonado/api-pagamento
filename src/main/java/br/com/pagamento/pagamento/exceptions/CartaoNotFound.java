package br.com.pagamento.pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartão não encontrado.")
public class CartaoNotFound extends RuntimeException{
}
