package br.com.pagamento.pagamento.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Pagamento {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   @NotNull(message = "Id do cartão não pode ser nulo")
   private Long cartao_id;

   @NotNull(message = "Descricao do produto não pode ser nulo")
   private String descricao;

   @NotNull(message = "Valor não pode ser nulo")
   private double valor;

   public Pagamento() {
   }

   public Long getId() {
       return id;
   }

   public void setId(Long id) {
       this.id = id;
   }

   public Long getCartao_id() {
       return cartao_id;
   }

   public void setCartao_id(Long cartao_id) {
       this.cartao_id = cartao_id;
   }

   public String getDescricao() {
       return descricao;
   }

   public void setDescricao(String descricao) {
       this.descricao = descricao;
   }

   public double getValor() {
       return valor;
   }

   public void setValor(double valor) {
       this.valor = valor;
   }
}
