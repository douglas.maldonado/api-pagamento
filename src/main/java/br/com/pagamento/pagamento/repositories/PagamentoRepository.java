package br.com.pagamento.pagamento.repositories;

import br.com.pagamento.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    Iterable<Pagamento> findAllByCartao_Id(Long cartao_id);
}
