package br.com.pagamento.pagamento.services;

import br.com.pagamento.pagamento.clients.CartaoClient;
import br.com.pagamento.pagamento.models.Cartao;
import br.com.pagamento.pagamento.models.Pagamento;
import br.com.pagamento.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento salvarPagamento(Pagamento pagamento){

        Cartao cartao = cartaoClient.getCartaoById(pagamento.getCartao_id());
        pagamento.setCartao_id(cartao.getId());

        Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);

        return pagamentoObjeto;
    }

    public Iterable<Pagamento> consultarPagamentosPorCartao(Long id){
        Iterable<Pagamento> pagamentoIterable = pagamentoRepository.findAllByCartao_Id(id);

        return pagamentoIterable;
    }

}
